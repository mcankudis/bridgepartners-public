'use strict';
const sanitize = {
  verifyWord: a => {
    if(a.length>1000000) return false;
    const regex = /[^a-z0-9]/i;
    return !regex.test(a);
  },
  verifyToken: a => {
    if(a.length>1000000) return false;
    const noDots = /\./;
    const dots = /\.[^\.]*\./;
    const twoDots = /\.\./;
    const overDots = /\.[^\.]*\.[^\.]*\./;
    const regex = /[^-_.a-z0-9]/i;
    if(regex.test(a)) return false;
    if(!noDots.test(a)) return false;
    if(twoDots.test(a)) return false;
    if(!dots.test(a)) return false;
    return !overDots.test(a);
  },
  verifyString: string => {
    if(typeof(string)!=="string") return false;
    if(string.length>100000) return false;
    const regex = /[\$\{\};]|(http:\/\/|https:\/\/)/i;
    return !regex.test(string);
  },
  verifyBool: value => {
    if(typeof value!== 'boolean') return false;
    return true;
  },
  verifyNumber: n => {
    if(typeof(n)==="number") return true;
    if(n.length>100000) return false;
    const regex = /[^-.0-9]/i;
    return !regex.test(n);
  },
  verifyEmail: email => {
    if(email.length>1000) return false;
    const doubleAt = /@[^@]*@/;
    const at = /@/;
    const dot = /\./;
    const regex = /[^-@._a-zA-Z0-9]/;
    if(regex.test(email)) return false;
    if(!at.test(email)) return false;
    if(!dot.test(email)) return false;
    return !doubleAt.test(email);
  },
  verifyName: a => {
    if(a.length>200) return false;
    const regex = /[^-a-zA-Z'ąćęłńóśżź\ ]/i;
    return !regex.test(a);
  }
}

module.exports = sanitize
