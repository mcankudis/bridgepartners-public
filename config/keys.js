'use strict';
const getRandomString = require('../functions/other').getRandomString;

const CronJob = require('cron').CronJob;
new CronJob('0 0 12 * * *', function() {
  process.env.SECRET = getRandomString(50,60);
  process.env.SECRET2 = getRandomString(50,60);
  console.log(process.env.SECRET);
  console.log(process.env.SECRET2);
}, null, true, 'America/Los_Angeles');

module.exports = {
  tokenAuth: {
    rerollSecret2: function() {
      process.env.SECRET2 = getRandomString(50,60);
    }
  }
};
