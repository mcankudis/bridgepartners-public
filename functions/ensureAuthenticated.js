'use strict';
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const sanitize = require('../config/sanitize');

module.exports = function(req, res, next) {
  if(!sanitize.verifyString(req.headers['x-auth'])) return res.status(403).send("Unauthorized");
  if(req.headers['x-auth']) {
    jwt.verify(req.headers['x-auth'], process.env.SECRET, function(err, token) {
      if(err) {
        res.sendStatus(403);
        return false;
      }
      else {
        req.auth = token;
        return next();
      }
    });
  } else {
    res.sendStatus(403);
    return false;
  }
}
