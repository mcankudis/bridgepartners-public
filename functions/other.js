"use strict";
const sanitize = require('../config/sanitize');

let isUserInChat = function(req, res, next) {
  if(req.locals.chat.players.indexOf(req.auth.id)!=-1) {
    next();
  } else return res.status(401).send("You are not in this chatroom");
}

let getRandomString = (mini, maxi) => {
  let tmp = Math.ceil(Math.random()*(maxi-mini))+mini;
  let helper = "1234567890abcdefghijklmnopqrstuvwxyz";
  let string = '';
  for (let i = 0; i < tmp; i++) {
    let rand = Math.floor(Math.random()*30);
    string+=helper[rand];
  }
  return string;
}

const verifyHand = (req, res, next) => {
	if(!sanitize.verifyWord(req.body.chat)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyString(req.body.to)) return res.status(401).send("Invalid data");
	req.body.CMAX1 = +(req.body.CMAX1),
	req.body.CMAX2 = +(req.body.CMAX2),
	req.body.CMIN1 = +(req.body.CMIN1),
	req.body.CMIN2 = +(req.body.CMIN2),
	req.body.DMAX1 = +(req.body.DMAX1),
	req.body.DMAX2 = +(req.body.DMAX2),
	req.body.DMIN1 = +(req.body.DMIN1),
	req.body.DMIN2 = +(req.body.DMIN2),
	req.body.HMAX1 = +(req.body.HMAX1),
	req.body.HMAX2 = +(req.body.HMAX2),
	req.body.HMIN1 = +(req.body.HMIN1),
	req.body.HMIN2 = +(req.body.HMIN2),
	req.body.PMAX1 = +(req.body.PMAX1),
	req.body.PMAX2 = +(req.body.PMAX2),
	req.body.PMIN1 = +(req.body.PMIN1),
	req.body.PMIN2 = +(req.body.PMIN2),
	req.body.SMAX1 = +(req.body.SMAX1),
	req.body.SMAX2 = +(req.body.SMAX2),
	req.body.SMIN1 = +(req.body.SMIN1),
	req.body.SMIN2 = +(req.body.SMIN2);
	
	let CMAX1 = +(req.body.CMAX1),
			CMAX2 = +(req.body.CMAX2),
			CMIN1 = +(req.body.CMIN1),
			CMIN2 = +(req.body.CMIN2),
			DMAX1 = +(req.body.DMAX1),
			DMAX2 = +(req.body.DMAX2),
			DMIN1 = +(req.body.DMIN1),
			DMIN2 = +(req.body.DMIN2),
			HMAX1 = +(req.body.HMAX1),
			HMAX2 = +(req.body.HMAX2),
			HMIN1 = +(req.body.HMIN1),
			HMIN2 = +(req.body.HMIN2),
			PMAX1 = +(req.body.PMAX1),
			PMAX2 = +(req.body.PMAX2),
			PMIN1 = +(req.body.PMIN1),
			PMIN2 = +(req.body.PMIN2),
			SMAX1 = +(req.body.SMAX1),
			SMAX2 = +(req.body.SMAX2),
			SMIN1 = +(req.body.SMIN1),
			SMIN2 = +(req.body.SMIN2);

	if(!sanitize.verifyNumber(CMAX1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(CMAX2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(CMIN1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(CMIN2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(DMAX1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(DMAX2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(DMIN1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(DMIN2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(HMAX1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(HMAX2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(HMIN1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(HMIN2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(SMAX1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(SMAX2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(SMIN1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(SMIN2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(PMAX1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(PMAX2)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(PMIN1)) return res.status(401).send("Invalid data");
	if(!sanitize.verifyNumber(PMIN2)) return res.status(401).send("Invalid data");
	let err_more_than_13 = 
		"Enter valid card number criteria. The minimal number of cards can't exceed 13";
	let err_less_than_0 = 
		"Enter valid card number criteria. The minimal number of cards can't be lower than 0";
	let err_min_over_max = 
		"Enter valid card number criteria. The minimal number of cards can't be higher that the maximal";
	let err_sum_lower_than_13 = 
		"Enter valid card number criteria. The sum of maximal numbers of cards in hand can't be lower than 13";
	let err_color_over_13 = 
		"Enter valid card number criteria. The sum of minimal numbers in a single color can't exceed 13";
	let err_points = "Enter valid points criteria";
	if(CMAX1>13||CMAX2>13||DMAX1>13||DMAX2>13||HMAX1>13||HMAX2>13||SMAX1>13||SMAX2>13)
		return res.status(404).send(err_color_over_13);
	if(CMAX1<0||CMAX2<0||DMAX1<0||DMAX2<0||HMAX1<0||HMAX2<0||SMAX1<0||SMAX2<0)
		return res.status(404).send(err_less_than_0);
	if(CMIN1<0||CMIN2<0||DMIN1<0||DMIN2<0||HMIN1<0||HMIN2<0||SMIN1<0||SMIN2<0)
		return res.status(404).send(err_less_than_0);
	if(CMIN1+CMIN2>13||DMIN1+DMIN2>13||HMIN1+HMIN2>13||SMIN1+SMIN2>13)
		return res.status(404).send(err_color_over_13);
	if(CMIN1>CMAX1||CMIN2>CMAX2||DMIN1>DMAX1||DMIN2>CMAX2||HMIN1>HMAX1||HMIN2>HMAX2||SMIN1>SMAX1||SMIN2>SMAX2) return res.status(404).send(err_min_over_max);
	if(CMIN1+DMIN1+HMIN1+SMIN1>13) return res.status(404).send(err_more_than_13);
	if(CMIN2+DMIN2+HMIN2+SMIN2>13) return res.status(404).send(err_more_than_13);
	if(CMAX1+DMAX1+HMAX1+SMAX1<13) return res.status(404).send(err_sum_lower_than_13);
	if(CMAX2+DMAX2+HMAX2+SMAX2<13) return res.status(404).send(err_sum_lower_than_13);
	if(CMIN1+CMIN2>13) return res.status(404).send(err_color_over_13);
	if(DMIN1+DMIN2>13) return res.status(404).send(err_color_over_13);
	if(HMIN1+HMIN2>13) return res.status(404).send(err_color_over_13);
	if(SMIN1+SMIN2>13) return res.status(404).send(err_color_over_13);
	if(PMIN1<0||PMIN2<0||PMIN1+PMIN2>40) return res.status(404).send(err_points);
	next();
}

module.exports = {
  isUserInChat, getRandomString, verifyHand
}