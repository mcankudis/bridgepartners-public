'use strict';

// Load enviroment variables
require('dotenv').config();

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const sockets = require('./websockets/socketio.js');
const bodyParser = require('body-parser');
const passport = require('passport');
const helmet = require('helmet');
const keys = require('./config/keys');
const passportSetup = require('./config/passport-setup');

// Security headers
app.use(helmet());

// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// initialize passport
app.use(passport.initialize());

//Set static folder
app.use(express.static(__dirname + '/client'));

// Set the view engine
app.set('views', __dirname + '/client');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// These are the routes
const index = require('./routes/index');
const chat = require('./routes/chats');
const boards = require('./routes/boards');
app.use('/', index);
app.use('/chat', chat);
app.use('/boards', boards);

const port = process.env.PORT || 3000;
server.listen(port, function() {
  console.log('['+new Date+'] Hi, listening on port ' + port);
})

sockets.init(server);
