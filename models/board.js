"use strict";
const db = require('../db');
const bcrypt = require('bcryptjs');
const Schema = db.Schema;

const boardSchema = new Schema({
  title:        {type: String, default: "Board", required: false},
  chat:         {type: String, required: true, index: true, unique: false},
  dealer:       {type: String, required: true},
  otherPos:     {type: String, required: true},
  isOver:       {type: Boolean, default: false, required: false},
  archived:     {type: Boolean, default: false, required: false},
  sequence:     {type: Array, required: false},
  hands: {
    w: {
      s: {type: String},
      h: {type: String},
      d: {type: String},
      c: {type: String},
      player: {type: String, default: ""}
    },
    n: {
      s: {type: String},
      h: {type: String},
      d: {type: String},
      c: {type: String},
      player: {type: String, default: ""}
    },
    e: {
      s: {type: String},
      h: {type: String},
      d: {type: String},
      c: {type: String},
      player: {type: String, default: ""}
    },
    s: {
      s: {type: String},
      h: {type: String},
      d: {type: String},
      c: {type: String},
      player: {type: String, default: ""}
    }
  }
});
const Board = db.model('board', boardSchema);
module.exports = Board;

module.exports.findById = id => {
  return new Promise((resolve, reject) => {
    Board.findOne({_id: id}, (err, board) => {
      if(err) {
        console.log(err);
        return reject({code: 401, msg: "An error occured"});
      }
      if(!board) return reject({code: 404, msg: "Board not found"});
      return resolve(board);
    })
  })
}
module.exports.findByChat = id => {
  return new Promise((resolve, reject) => {
    Board.find({chat: id}, (err, boards) => {
      if(err) {
        console.log(err);
        return reject({code: 401, msg: "An error occured"});
      }      
      if(!boards) return reject({code: 404, msg: "Boards not found"});
      return resolve(boards);
    })
  })
}
module.exports.prepareForPlayer = (boards, id) => {
  for (var i = 0; i < boards.length; i++) {
    if(boards[i].hands.n.player==id) {
      boards[i].myPosition = 'n';
      boards[i].hand = {...boards[i].hands.n, pos: "n"};
      if(boards[i].isOver) boards[i].hand2 = {...boards[i].hands.s, pos: "s"};
      boards[i].hands = undefined;
    }
    else if(boards[i].hands.s.player==id) {
      boards[i].myPosition = 's';
      boards[i].hand = {...boards[i].hands.s, pos: "s"};
      if(boards[i].isOver) boards[i].hand2 = {...boards[i].hands.n, pos: "n"};
      boards[i].hands = undefined;
    }
  }
  return boards;
}
module.exports.createFromHands = (hands, chat, title) => {
  let board = new Board({
    title: title || "board",
    chat: chat,
    hands: {
      w: {},
      n: {},
      e: {},
      s: {}
    }
  })
  board.hands.n.s = hands[0];
  board.hands.n.h = hands[1];
  board.hands.n.d = hands[2];
  board.hands.n.c = hands[3];
  board.hands.w.s = hands[4];
  board.hands.w.h = hands[5];
  board.hands.w.d = hands[6];
  board.hands.w.c = hands[7];
  board.hands.e.s = hands[8];
  board.hands.e.h = hands[9];
  board.hands.e.d = hands[10];
  board.hands.e.c = hands[11];
  board.hands.s.s = hands[12];
  board.hands.s.h = hands[13];
  board.hands.s.d = hands[14];
  board.hands.s.c = hands[15];
  board.sequence = [];
  return board;
}
