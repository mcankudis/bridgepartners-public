"use strict";
const db = require('../db');
const Schema = db.Schema;

const psubSchema = new Schema({
	username:        {type: String, required: true, index: true, unique: true},
	user_id:         {type: String, required: true, index: true, unique: true},
	subscription:    {type: Object, required: true}
});

const PushSubscription = db.model('pushSubscription', psubSchema);
module.exports = PushSubscription;

module.exports.findByUserId = id => {
	return new Promise((resolve, reject) => {
		PushSubscription.findOne({user_id: id}, (err, psub) => {
			console.log(err, psub);
			if(err) return reject({code: 401, msg: "An error occured"});
			if(!chat) return reject({code: 404, msg: "Device not found"});
			return resolve(psub);
		})
	})
}
module.exports.findByUsername = name => {
	return new Promise((resolve, reject) => {
		PushSubscription.findOne({username: name}, (err, psub) => {
			console.log(err, psub);
			if(err) return reject({code: 401, msg: "An error occured"});
			if(!psub) return reject({code: 404, msg: "Device not found"});
			return resolve(psub);
		})
	})
}