"use strict";
const db = require('../db');
const bcrypt = require('bcryptjs');
const Schema = db.Schema;

const userSchema = new Schema({
    username:   {type: String, required: true, unique: true, index: true},
    password:   {type: String, required: true},
    email:      {type: String, required: true, unique: true},
    reissue_id: {type: String, required: false}
    // googleId:   {type: String, required: false, unique: true},
    // thumbnail:  {type: String, required: false, unique: false},
});

const User = db.model('user', userSchema);
module.exports = User;

// callback functions are deprecated
module.exports.changePasswd = function(id, newPassword, callback) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newPassword, salt, function(err, hash) {
      if(err) {throw err;}
      User.updateOne({_id: id}, {$set: {password: hash}}, callback);
    });
  });
}
module.exports.changeUsername = function(id, newUsername, callback) {
  User.updateOne({_id: id}, {$set: {username: newUsername}}, callback);
}
module.exports.getUserByUsername = function(username, callback) {
  var query = {username: username};
  User.findOne(query, callback);
}
module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
}
module.exports.comparePassword = function(newPassword, hash, callback) {
  bcrypt.compare(newPassword, hash, function(err, isMatch) {
    if(err) {throw err;}
    callback(null, isMatch);
  })
}

// promise functions
module.exports.createUser = newUser => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if(err) return reject(err);
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if(err) return reject(err)
        newUser.password = hash;
        newUser.save((err, user) => {
          if(err) return reject(err);
          else return resolve(user);
        });
      });
    });
  })
}

module.exports.byId = id => {
  return new Promise((resolve, reject) => {
    User.findOne({_id: id}, (err, user) => {
      if(err) {
        console.log(err);
        return reject({code: 401, msg: "An error occured"});
      }
      if(!user) return reject({code: 404, msg: "User not found"});
      return resolve(user);
    })
  })
}
