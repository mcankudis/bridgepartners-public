"use strict";
const db = require('../db');
const bcrypt = require('bcryptjs');
const Schema = db.Schema;

const chatSchema = new Schema({
    customName:   {type: String, required: false},
    players:      {type: Array, required: true},
    // fixed WNES?
    spectators:   {type: Array, required: false},
    thumbnail:    {type: String, required: false},
});

const Chat = db.model('chat', chatSchema);
module.exports = Chat;
