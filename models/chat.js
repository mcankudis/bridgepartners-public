"use strict";
const db = require('../db');
const bcrypt = require('bcryptjs');
const Schema = db.Schema;

const chatSchema = new Schema({
	customName:   {type: String, required: false},
	pId_1:        {type: String, required: true, index: true, unique: true},
	pId_2:        {type: String, required: true, index: true, unique: true},
	players:      {type: Array, required: true},
	spectators:   {type: Array, required: false},
	thumbnail:    {type: String, required: false}
});

const Chat = db.model('chat', chatSchema);
module.exports = Chat;

module.exports.findById = (id) => {
	return new Promise((resolve, reject) => {
		Chat.findOne({_id: id}, (err, chat) => {
			if(err) return reject({code: 500, msg: "An error occured"});
			if(!chat) return reject({code: 404, msg: "Chat not found"});
			return resolve(chat);
		})
	})
}
