let insertColor = function(i, j) {
  let text = $('#bid'+i).val();
  let x = document.getElementById('bid'+i).selectionStart;
  let sign;
  switch (j) {
    case 1:
      sign = '♣';
      break;
    case 2:
      sign = '♦';
      break;
    case 3:
      sign = '♥';
      break;
    case 4:
      sign = '♠';
      break;
    default: M.toast('Wystąpił błąd', 4000);
  }
  console.log(x);
  let t = text.slice(0,x) + sign + text.slice(x,text.length);
  $('#bid'+i).val(t);
}
// body = body.slice(9476, body.length-6517)

`<div class="btn btn-floating green" onclick="insertColor(${i}, 1)">♣</div>
<div class="btn btn-floating orange" onclick="insertColor(${i}, 2)">♦</div>
<div class="btn btn-floating red" onclick="insertColor(${i}, 3)">♥</div>
<div class="btn btn-floating blue" onclick="insertColor(${i}, 4)">♠</div>`
