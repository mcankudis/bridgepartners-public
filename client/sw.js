"use strict";
const VERSION = '0.0.36';
const cacheBase = 'cache-base';
const resToPrecache = [
  '/',
  'images/logo.png',
  'images/favicon.png',
  'views/index.html',
  'views/login.html',
  'views/register.html',
  'views/changepass.html',
  'views/guest.html',
  'styles/style.css',
  'styles/nouislider.css',
  'scripts/app.js',
  'scripts/app-login.js',
  'scripts/app-register.js',
  'scripts/app-changepass.js',
  'scripts/app-guest.js',
  'scripts/functions.js',
  'scripts/nouislider.js',
  'scripts/sockets.js',
  'https://code.jquery.com/jquery-3.3.1.min.js ',
  'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css',
  'https://fonts.googleapis.com/icon?family=Material+Icons'
]

self.addEventListener('install', event => {
  console.log("installed");
  event.waitUntil(
    caches.open(cacheBase)
    .then(cache => {
      return cache.addAll(resToPrecache);
    })
  )
})

self.addEventListener('activate', event => {
  console.log("activated");
})

self.addEventListener('fetch', event => {
  if(event.request.method==="POST") return false;
  if(event.request.url.includes('socket.io')) return false;
  if(event.request.url.includes('reissue')) return false;
  if(event.request.url.includes('boards')) {
    fetchNetworkFirst(event, 'cache-boards')
  }
  else if(event.request.url.includes('chat')) {
    fetchNetworkFirst(event, 'cache-chat')
  }
  // soon there will be a better strategy for different types of ressources
  else {
    event.respondWith(
      caches.open(cacheBase).then(cache => {
        return cache.match(event.request).then(response => {
          return response || fetch(event.request).then(response => {
            cache.put(event.request, response.clone());
            return response;
          });
        });
      })
    );
  }
});

self.addEventListener('push', async event => {
  let data = await event.data.json();
  const body = data.msg;
  const icon = "/images/favicon.png";
  const tag = "Bridge Partners";
  self.registration.showNotification(data.title, {
    body: body,
    icon: icon,
    tag: tag
  })
})

let fetchNetworkFirst = (event, cacheName) => {
  console.log(cacheName);
  event.respondWith(
    caches.open(cacheName).then(cache => {
      return fetch(event.request)
      .then(response => {
        console.log(response);
        if(!response.ok) {
          if(response.status===403) return response;
          return cache.match(event.request)
          .then(response => response)
          .catch(() => {
            console.log("Here I am in the catch! Nothing in cache" + arguments)
            return {};
          })
        }
        else {
          cache.put(event.request, response.clone());
          return response;
        }
      })
      .catch((err) => {
        console.log(err);
        return cache.match(event.request)
        .then(response => response)
        .catch(() => {
          console.log("Here I am in the catch! Nothing in cache 2" + arguments)
          return {};
        })
      })
    })
  )
}
