let url_string = window.location.href;
let url = new URL(url_string);
let token = url.searchParams.get("token");

let displayRoom = () => {
  let o = $('#openRoom');
  $.post("/boards/guest", {
    token
  })
  .done(data => {
    let players = [];
    for (let i = 0; i < data.players.length; i++) {
      players[data.players[i]._id] = data.players[i].username;
    }
    let boards = data.boards;
    let room =
    `<div class="row">
      <div class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="card blue-grey darken-1">
              <div class="card-content white-text">
                <h5>Auction between ${data.players[0].username} and ${data.players[1].username}</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="row" id="boardList">`
    if(boards.length==0) {
      room+= `<div class="col s12 marginI20" id="noBoards">
                <div class="card blue-grey darken-1">
                  <h6 class="card-content white-text">It seems there are no boards in this room</h6>
                </div>
              </div>`
    }
    for (var i = 0; i < boards.length; i++) {
      room += boardToGuestHtml(boards[i], players);
    }
    room+=
          `</div>
        </div>
    </div>`
    o.empty();
    o.append(room);
    o.slideDown("slow");
  })
  .fail(err => handleErr(err))
}

let boardToGuestHtml = (data, players) => {
  let room = "";
  room+= `<div class="col s12 m6 l4" id="boardCard${data._id}">
            <div class="card blue-grey">
              <div class="card-content white-text">
                <div>
                  <div class="card-title">${data.title}</div>
                </div>
                <div class="row">`
                  let nHand = `
                  <div class="col s6 marginB1 white-text">
                    <div>N: ${players[data.hands.n.player]}</div>
                    <div><span class="spade">&#9824;</span> ${data.hands.n.s}</div>
                    <div><span class="heart">&#9829;</span> ${data.hands.n.h}</div>
                    <div><span class="diamond">&#9830;</span> ${data.hands.n.d}</div>
                    <div><span class="club">&#9827;</span> ${data.hands.n.c}</div>
                  </div>`
                  let sHand = `
                  <div class="col s6 white-text">
                    <div>S: ${players[data.hands.s.player]}</div>
                    <div><span class="spade">&#9824;</span> ${data.hands.s.s}</div>
                    <div><span class="heart">&#9829;</span> ${data.hands.s.h}</div>
                    <div><span class="diamond">&#9830;</span> ${data.hands.s.d}</div>
                    <div><span class="club">&#9827;</span> ${data.hands.s.c}</div>
                  </div>`
                  if(data.dealer=='n') room+=nHand+sHand;
                  else room+=sHand+nHand;
                  room+=`<div class="col s12">
                  <ul class="black-text row">`
                  let n,s;
                  if(data.dealer=='n') {
                    n = `<li class="col s6 biddingHeading red lighten-2">N</li>`;
                    s = `<li class="col s6 biddingHeading yellow lighten-2">S</li>`;
                    room+=n+s;
                  }
                  else {
                    n = `<li class="col s6 biddingHeading yellow lighten-2">N</li>`;
                    s = `<li class="col s6 biddingHeading red lighten-2">S</li>`;
                    room+=s+n;
                  }
                  for (var j = 0; j < data.sequence.length; j++) {
                    let color;
                    if(j%2==0) color = 'red lighten-3';
                    else color = 'yellow lighten-3';
                    let x = getClassForBid(data.sequence[j]);
                    let sign = x[0], c = x[1];
                    let bidHtml = `<li class="col s6 biddingBid ${color}">
                      ${data.sequence[j][0]}<span class='${c}'>${sign} </span>
                    </li>`;
                    room+=bidHtml
                  }
                  room+=
                    `</ul>
                  </div>
                </div>
              </div>
            </div>
          </div>`
  return room;
}

displayRoom();
