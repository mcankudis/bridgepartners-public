if('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js')
    .then(reg => {
      console.log(`This app uses service worker`);
      console.log(reg);
    })
    .catch(err => {
      console.log(`Failed to run the service worker`);
      console.log(err);
    })
  })
}

$(document).ready(function() {
  let defferedPrompt;
  let btnAdd = document.getElementById('btnAdd');
  window.addEventListener('beforeinstallprompt', e => {
    e.preventDefault();
    defferedPrompt = e;
    btnAdd.style.display = "block";
  })
  btnAdd.addEventListener('click', e => {
    defferedPrompt.prompt();
    defferedPrompt.userChoice
    .then(res => {
      if(res.outcome === "accepted") console.log('acc');
      defferedPrompt = null;
    })
  })
  let loginForm = $('#loginForm');
  loginForm.submit(function(e) {
    let username = $('#username').val();
    let password = $('#password').val();
    e.preventDefault();
    $.post("/login", {username:username, password:password})
    .done(function(data) {
      localStorage.setItem('token', data.token);
      localStorage.setItem('reissueToken', data.reissueToken);
      localStorage.setItem('username', data.user.username);
      localStorage.setItem('_id', data.user._id);
      M.toast({html:"Login successful"}, 4000)
      window.location.href="/";
    })
    .fail(function(err) {
      M.toast({html:err.responseText}, 4000)
    })
  })
})

let forgotPassword = () => {
  $('#loginBody').hide();
  $('#forgotPassword').fadeIn();
}

let forgotPasswordReturn = () => {
  $('#forgotPassword').hide();
  $('#loginBody').fadeIn();
}

let submitResetPass = () => {
  let email = $('#email').val()
  $.post("/resetPassword", {email: email})
  .done(function(data) {
    M.toast({html:"An email with further instructions has been sent"}, 4000)
  })
  .fail(function(err) {
    M.toast({html:err.responseText}, 4000)
  })
}

let forgotPasswordForm = $('#forgotPasswordForm');
forgotPasswordForm.submit((e) => {
  e.preventDefault();
  console.log("XDS");
  submitResetPass();
})
