"use strict";
let chats = [];
let currentChat = {};
let currentBoard = {};
let boards = [];
let socket, user;
let publicVapidKey = "";

let token = localStorage.getItem('token');
if(!token) window.location.href = "/login";
user = {
  username: localStorage.getItem('username')
}
$.ajaxSetup({
  headers: { 'x-auth': token }
});

fetch('/ping')
.then(() => {
  socket = io() || {};
  socket.emit('attachUser', user.username);
  socketsListen();
})
.catch(() => {
  console.log("offline state");
})

let defferedPrompt;
let btnAdd = document.getElementById('btnAdd');
window.addEventListener('beforeinstallprompt', e => {
  e.preventDefault();
  defferedPrompt = e;
  btnAdd.style.display = "block";
})
btnAdd.addEventListener('click', e => {
  defferedPrompt.prompt();
  defferedPrompt.userChoice
  .then(res => {
    if(res.outcome === "accepted") console.log('acc');
    defferedPrompt = null;
  })
})

getChats();

window.isUpdateAvailable = new Promise(async (resolve, reject) => {
  let r = await fetch('/publicVapidKey');
  if(!r || !r.ok) reject(false);
  publicVapidKey = await r.text();
	if (!'serviceWorker' in navigator) reject(false);
	let reg = await navigator.serviceWorker.register('/sw.js');
  if(!reg) reject(false);
  if (!("Notification" in window)) alert("This browser does not support push notifications");
  else if (Notification.permission !== "denied") Notification.requestPermission();
  let sub = await reg.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
  })
  if(!sub) reject(false);
  r = await fetch('/subscribe', {
    method: "POST",
    body: JSON.stringify(sub),
    headers: setHeaders({
      'content-type': 'application/json'
    })
  })
	reg.onupdatefound = () => {
		const installingWorker = reg.installing;
		installingWorker.onstatechange = () => {
			switch (installingWorker.state) {
				case 'installed':
					if (navigator.serviceWorker.controller) return resolve(true);
					return resolve(false);
			}
		};
	};
});

window['isUpdateAvailable']
.then(isAvailable => {
  if (isAvailable) M.toast({html:"The app has just been updated! Reload it to start using new version!"}, 4000);
})
.catch(() => console.log("no update"))

// DOM SETUP
$(() => {
  $('#username').html(user.username);
  $('.sidenav').sidenav();
  $('.modal').modal();
  $(".dropdown-trigger").dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false,
      coverTrigger: false,
      alignment: 'right',
      stopPropagation: true
  });
  $('.bottom-sheet').modal({
    opacity: 0,
    preventScrolling: false
  });
  noUiSlider.create($('#linkTime')[0], {
    start: [0],
    range: {
        'min': [1],
        '10%': [4],
        '20%': [9],
        '30%': [16],
        '40%': [25],
        '50%': [36],
        '60%': [49],
        '70%': [64],
        '80%': [81],
        '90%': [100],
        'max': [121]
    },
    format: {
     to: (value) => Math.round(value)+'h',
     from: (value) => value
    }
  });
  $('[id*="slider"]').each((i, obj) => {
    noUiSlider.create(obj, {
     start: [0, 13],
     connect: true,
     step: 1,
     orientation: 'horizontal',
     range: {
       'min': 0,
       'max': 13
     },
     format: {
      to: (value) => Math.round(value),
      from: (value) => value
     }
   });
  })
  $('[id*="pointsSlider"]').each((i, obj) => {
    noUiSlider.create(obj, {
     start: [0, 37],
     connect: true,
     step: 1,
     orientation: 'horizontal',
     range: {
       'min': 0,
       'max': 37
     },
     format: {
        to: value => Math.round(value),
        from: value => value
    },
     pips: {
         mode: 'values',
         values: [0, 5, 10, 15, 20, 25, 30, 37],
         density: 3,
         stepped: true
     }
    });
  })
}) // EOF DOM SETUP

let addBid = (bid) => {
  let board = {...currentBoard};
  $.post("/boards/addBid", {
    board: board._id,
    bid: bid,
    to: currentChat.partner,
    chat: currentChat._id
  })
  .done((data) => {
    M.toast({html:"Bid added"}, 4000);
    $('#biddingBoxModal').modal('close');
    let i = boards.map(x=>x._id).indexOf(data.id)
    boards[i].sequence.push(bid);
    let x = getClassForBid(bid);
    let sign = x[0], c = x[1];
    let color;
    if(boards[i].sequence.length%2==1) color = 'red lighten-3';
    else color = 'yellow lighten-3';
    let bidHTML = `<li class="col s6 biddingBid ${color}">${bid[0]}<span class='${c}'>${sign} </span></li>`;
    $('#auction'+data.id).append(bidHTML);
    $('#addBoardModal').modal('close');
    if(bid=="pass" && boards[i].sequence.length>1) endBoard(data.id);
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => {
      console.log(code);
      console.log(bid);
      code===2 ? addBid(bid) : true
    })
  )
}

let archiveBoard = (id) => {
  $.post("/boards/archive", {
    board: id,
    chat: currentChat._id,
    to: currentChat.partner
  })
  .done(() => {
    let i = boards.map(x => x._id).indexOf(id);
    boards.splice(i,1);
    M.toast({html:"Board archived"}, 4000);
    $('#archiveButton'+id).tooltip('destroy');
    $('#boardCard'+id).fadeOut();
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? archiveBoard(id) : true)
  )
}

let changePassword = () => {
  let passwordOld = $('#passwordOld').val();
  let newPassword = $('#passwordNew').val();
  let newPassword2 = $('#passwordNew2').val();
  if(newPassword!==newPassword2) return M.toast({html:"Passwords do not match"}, 4000);
  $.post("/changePassword", {
    newPassword: newPassword,
    password: passwordOld
  })
  .done(() => {
    M.toast({html:"Password changed", classes: 'green'}, 4000);
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? changePassword() : true)
  )
}

let changeUsername = () => {
  let password = $('#passwordUChange').val();
  let newUsername = $('#newUsername').val();
  $.post("/changeUsername", {
    newUsername: newUsername,
    password: password
  })
  .done(data => {
    M.toast({html:"Username changed", classes: 'green'}, 4000);
    localStorage.setItem('username', data.username);
    localStorage.setItem('token', data.token);
    $('#username').html(data.username);
    $.ajaxSetup({
      headers: { 'x-auth': data.token }
    });
    user.username = data.username;
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? changeUsername(bid) : true)
  )
}

let closeArchive = () => {
  let b = $('#boardList');
  b.empty();
  let room = "";
  for (let i = 0; i < boards.length; i++) {
    if(!boards[i].archived) room += boardToHtml(boards[i]);
  }
  $('#closeArchive').remove();
  $('#roomHeader').append(`
    <a id="openArchive" onclick="openArchive()"
    class="btn-floating btn-large waves-effect waves-light yellow darken-2 right">
      <i class="material-icons">archive</i>
    </a>
  `)
  if(room=="")
    room = `<div class="col s12 marginI20" id="noBoards">
              <div class="card blue-grey darken-1">
                <h6 class="card-content white-text">
                  It seems there are no active boards in this room. Add one using the button above.
                </h6>
              </div>
            </div>`
  b.append(room);
}

let endBoard = (id) => {
  $.post("/boards/end", {
    board: id,
    chat: currentChat._id,
    to: currentChat.partner
  })
  .done((data) => {
    let o = $('#otherHand'+id);
    o.append(
      `<div class="col s12 m6">
        <div><span class="spade">&#9824;</span> ${data.hand.s}</div>
        <div><span class="heart">&#9829;</span> ${data.hand.h}</div>
        <div><span class="diamond">&#9830;</span> ${data.hand.d}</div>
        <div><span class="club">&#9827;</span> ${data.hand.c}</div>
      </div>`);
    $('#overButton'+id).tooltip('destroy');
    $('#overButton'+id).remove();
    $('#boardButtons'+id).append(
      `<a class="btn-floating waves-effect waves-light yellow darken-2 right tooltipped marginR2"
      data-position="top" data-tooltip="Archive board" id="archiveButton${data._id}"
      onclick="archiveBoard('${data._id}')"><i class="material-icons">archive</i></a>`)
    $('.tooltipped').tooltip();
    M.toast({html:"Auction is over, You can now see both hands"}, 4000);
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? endBoard(id) : true)
  )
}

let genShareLink = () => {
  let chat = currentChat._id;
  let exp = linkTime.noUiSlider.get();
  let acc = $('input[name="access"]:checked').val();
  if($('#always').is(':checked')) exp = '0';
  $.post('/boards/generateGuestLink', {chat, exp, acc})
  .done((link) => {
    $('#linkContainer').text(link);
    $('#shareLinkModal').modal('open');
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? genShareLink() : true)
  )
}

let openBiddingBox = (id) => {
  let i = boards.map(x => x._id).indexOf(id);
  if(boards[i].isOver) return;
  if(boards[i].dealer==boards[i].myPosition && boards[i].sequence.length%2 !== 0) return;
  if(boards[i].dealer!=boards[i].myPosition && boards[i].sequence.length%2 === 0) return;
  currentBoard = boards[i];
  $('#biddingBoxModal').modal('open');
}

let openAddBoard = () => {
  $('#addBoardModal').modal('open');
}

let openArchive = () => {
  let b = $('#boardList');
  b.empty();
  let room = "";
  for (let i = 0; i < boards.length; i++) {
    if(boards[i].archived) room += boardToHtml(boards[i]);
  }
  $('#openArchive').remove();
  $('#roomHeader').append(`
    <a id="closeArchive" onclick="closeArchive()"
    class="btn-floating btn-large waves-effect waves-light yellow darken-2 right">
      <i class="material-icons">arrow_back</i>
    </a>
  `)
  if(room=="")
    room = `<div class="col s12 marginI20" id="noBoards">
              <div class="card blue-grey darken-1">
                <h6 class="card-content white-text">
                  It seems there are no archived boards in this room.
                </h6>
              </div>
            </div>`
  b.append(room);
  $('.tooltipped').tooltip();
}

let openGenShareLink = () => {
  if($('#always').is(':checked')) $('#linkTimeForm').fadeOut();
  $('#genShareLinkModal').modal('open');
}

let openRoom = (i) => {
  currentChat = chats[i];
  let o = $('#openRoom');
  $('#noRoom').addClass('hidden');
  o.fadeOut("fast");
  $.get(`/boards/boardsByChatId/${chats[i]._id}`)
  .done(data => {
    boards = data;
    let noBoardsCard = "", pulse = "";
    if(data.filter(x=>!x.archived).length==0) {
      noBoardsCard+= `<div class="col s12 marginI20" id="noBoards">
                <div class="card blue-grey darken-1">
                  <h6 class="card-content white-text">
                    It seems there are no active boards in this room. Add one using the button above.
                  </h6>
                </div>
              </div>`
      pulse = "pulse";
    }
    let room =
    `<div class="row">
      <div class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="card blue-grey darken-1">
              <div class="card-content white-text" id="roomHeader">
                <h5>Auction with ${currentChat.customName}</h5>
                <a id="openAddBoard" onclick="openAddBoard()"
                class="btn-floating btn-large waves-effect waves-light green right ${pulse}">
                  <i class="material-icons">add</i>
                </a>
                <a id="openGenShareLink" onclick="openGenShareLink()"
                class="btn-floating btn-large waves-effect waves-light blue right">
                  <i class="material-icons">share</i>
                </a>
                <a id="openArchive" onclick="openArchive()"
                class="btn-floating btn-large waves-effect waves-light yellow darken-2 right">
                  <i class="material-icons">archive</i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="row" id="boardList">`
    room+=noBoardsCard;
    for (let i = 0; i < data.length; i++) {
      if(!data[i].archived) room += boardToHtml(data[i]);
    }
    room+=
          `</div>
        </div>
    </div>`
    o.empty();
    o.append(room);
    o.slideDown("fast");
    $('#genShareLinkModal').modal();
    $('.tooltipped').tooltip();
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => {
      if(code===2) return openRoom(i);
      let o = $('#openRoom');
      $('#noRoom').addClass('hidden');
      o.fadeOut("fast");
      let networkErrorCard =
        `<div class="col s12 marginT20">
          <div class="card blue-grey darken-1">
            <h6 class="card-content white-text">
              Cannot estabilish connection to the server and find this room in cache. Enable internet connection and reload the room.
            </h6>
          </div>
        </div>`
      o.empty();
      o.append(networkErrorCard);
      o.slideDown("fast");
    })
  )
}

let refreshChats = (chats) => {
  let chatList;
  if(window.innerWidth>992) chatList = $('#chatList');
  else chatList = $('#chatListMobile');
  chatList.empty();
  chatList.append(
    `<li class="collection-header"><h4>Auction rooms</h4>
      <form id="newChatForm">
          <div class="row">
            <div class="input-field col s6">
              <input id="newChatName" type="text">
              <label for="newChatName">Name</label>
            </div>
            <div class="input-field col s6">
              <input type="submit" class="btn" value="Add" id="addNewChat"/>
            </div>
          </div>
        </form>
      </li>`);
  let newChatForm = $('#newChatForm');
  newChatForm.submit(e => {
    e.preventDefault();
    let name = $('#newChatName').val();
    addNewChat(name)
    .then(chat => {
      M.toast({html:"Auction room created"}, 4000);
      chats.push(chat);
      chatList.append(
        `<li class="collection-item" onclick="openRoom(${chats.length-1})">${chats[chats.length-1].customName}</li>`)
    })
    .catch(err =>
      handleErrAsync(err)
      .then(code => code===2 ? refreshChats() : true)
    )
  })
  let list = "";
  for (let i = 0; i < chats.length; i++) {
    let x = `<li class="collection-item" onclick="openRoom(${i})">${chats[i].customName}</li>`;
    list += x;
  }
  chatList.append(list);
}

let removeBoard = (id) => {
  $.post("/boards/remove", {
    board: id,
    chat_id: currentChat._id,
    to: currentChat.partner
  })
  .done(() => {
    let i = boards.map(x => x._id).indexOf(id);
    boards.splice(i,1);
    M.toast({html:"Board removed"}, 4000);
    $('#deleteButton'+id).tooltip('destroy');
    $('#boardCard'+id).remove();
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? removeBoard(id) : true)
  )
}

let undoBid = (board_id) => {
  if(boards[boards.map(x=>x._id).indexOf(board_id)].sequence.length<=0) return M.toast({html:"There is no bid to undo"}, 4000);
  $.post("/boards/undo", {
    board: board_id,
    to: currentChat.partner,
    chat: currentChat._id
  })
  .done((data) => {
    M.toast({html:"Undo successful"}, 4000);
    let i = boards.map(x=>x._id).indexOf(data.id)
    boards[i].sequence.pop();
    $(`#auction${board_id}`).children().last().remove();
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? undoBid(board_id) : true)
  )
}


let addBoard = () => {
  let CMAX1 =	sliderClubsN.noUiSlider.get()[1],
  CMAX2 =	sliderClubsS.noUiSlider.get()[1],
  CMIN1 =	sliderClubsN.noUiSlider.get()[0],
  CMIN2 =	sliderClubsS.noUiSlider.get()[0],
  DMAX1 =	sliderDiamondsN.noUiSlider.get()[1],
  DMAX2 =	sliderDiamondsS.noUiSlider.get()[1],
  DMIN1 =	sliderDiamondsN.noUiSlider.get()[0],
  DMIN2 =	sliderDiamondsS.noUiSlider.get()[0],
  HMAX1 =	sliderHeartsN.noUiSlider.get()[1],
  HMAX2 =	sliderHeartsS.noUiSlider.get()[1],
  HMIN1 =	sliderHeartsN.noUiSlider.get()[0],
  HMIN2 =	sliderHeartsS.noUiSlider.get()[0],
  PMAX1 =	pointsSliderN.noUiSlider.get()[1],
  PMAX2 =	pointsSliderS.noUiSlider.get()[1],
  PMIN1 =	pointsSliderN.noUiSlider.get()[0],
  PMIN2 =	pointsSliderS.noUiSlider.get()[0],
  SMAX1 =	sliderSpadesN.noUiSlider.get()[1],
  SMAX2 =	sliderSpadesS.noUiSlider.get()[1],
  SMIN1 =	sliderSpadesN.noUiSlider.get()[0],
  SMIN2 =	sliderSpadesS.noUiSlider.get()[0];
  let err_more_than_13 = "Enter valid card number criteria. The maximal number of cards can't exceed 13";
  let err_less_than_0 = "Enter valid card number criteria. The minimal number of cards can't be lower than 0";
  let err_points = "Enter valid points criteria";
  let err_min_over_max =
    "Enter valid card number criteria. The minimal number of cards can't be higher that the maximal";
  let err_color_over_13 =
    "Enter valid card number criteria. The sum of minimal numbers in a single color can't exceed 13";
  let err_sum_lower_than_13 =
    "Enter valid card number criteria. The sum of maximal numbers of cards in hand can't be lower than 13";
  if(CMAX1>13||CMAX2>13||DMAX1>13||DMAX2>13||HMAX1>13||HMAX2>13||SMAX1>13||SMAX2>13)
    return M.toast({html: err_more_than_13}, 4000);
  if(CMAX1<0||CMAX2<0||DMAX1<0||DMAX2<0||HMAX1<0||HMAX2<0||SMAX1<0||SMAX2<0)
    return M.toast({html:err_less_than_0}, 4000);
  if(CMIN1<0||CMIN2<0||DMIN1<0||DMIN2<0||HMIN1<0||HMIN2<0||SMIN1<0||SMIN2<0)
    return M.toast({html:err_less_than_0}, 4000);
  if(CMIN1>13||CMIN2>13||DMIN1>13||DMIN2>13||HMIN1>13||HMIN2>13||SMIN1>13||SMIN2>13)
    return M.toast({html:err_more_than_13}, 4000);
  if(CMIN1>CMAX1||CMIN2>CMAX2||DMIN1>DMAX1||DMIN2>CMAX2||HMIN1>HMAX1||HMIN2>HMAX2||SMIN1>SMAX1||SMIN2>SMAX2)
    return M.toast({html: err_min_over_max}, 4000);
  if(CMIN1+DMIN1+HMIN1+SMIN1>13) return M.toast({html: err_sum_lower_than_13}, 4000);
  if(CMIN2+DMIN2+HMIN2+SMIN2>13) return M.toast({html: err_sum_lower_than_13}, 4000);
  if(CMAX1+DMAX1+HMAX1+SMAX1<13) return M.toast({html: err_sum_lower_than_13}, 4000);
  if(CMAX2+DMAX2+HMAX2+SMAX2<13) return M.toast({html: err_sum_lower_than_13}, 4000);
  if(CMIN1+CMIN2>13) return M.toast({html:err_color_over_13}, 4000);
  if(DMIN1+DMIN2>13) return M.toast({html:err_color_over_13}, 4000);
  if(HMIN1+HMIN2>13) return M.toast({html:err_color_over_13}, 4000);
  if(SMIN1+SMIN2>13) return M.toast({html:err_color_over_13}, 4000);
  if(PMIN1<0||PMIN2<0||PMIN1+PMIN2>40) return M.toast({html: err_points}, 4000);
$.post("/boards/new", {
  CMAX1:	CMAX1,
  CMAX2:	CMAX2,
  CMIN1:	CMIN1,
  CMIN2:	CMIN2,
  DMAX1:	DMAX1,
  DMAX2:	DMAX2,
  DMIN1:	DMIN1,
  DMIN2:	DMIN2,
  HMAX1:	HMAX1,
  HMAX2:	HMAX2,
  HMIN1:	HMIN1,
  HMIN2:	HMIN2,
  PMAX1:	PMAX1,
  PMAX2:	PMAX2,
  PMIN1:	PMIN1,
  PMIN2:	PMIN2,
  SMAX1:	SMAX1,
  SMAX2:	SMAX2,
  SMIN1:	SMIN1,
  SMIN2:	SMIN2,
  position: $('[name="position"]:checked').val(),
  chat: currentChat._id,
  title: $('#title').val(),
  to: currentChat.partner
})
.done(data => {
  boards.push(data);
  let boardHtml = boardToHtml(data);
  $('#noBoards').remove();
  $('#boardList').append(boardHtml);
  $('#openAddBoard').removeClass('pulse');
  M.toast({html:"Board added"}, 4000);
  $('.tooltipped').tooltip();
})
.fail(err =>
  handleErrAsync(err)
  .then(code => code===2 ? addBoard() : true)
)
}

let toggleLinkTime = () => {
  let el = $("#linkTimeForm");
  if(el.is(':visible')) return el.fadeOut();
  else return el.fadeIn();
}
