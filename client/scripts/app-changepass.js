$(document).ready(function() {
  let passForm = $('#passForm');
  passForm.submit(function(e) {
    e.preventDefault();
    let url = new URL(document.location);
    let token = url.pathname.slice(7, url.pathname.length)
    let password = $('#password').val();
    let password2 = $('#password2').val();
    if(password!==password2) return M.toast({html: "Passwords are different"}, 4000);
    $.post("/reset/resetChangePass", {password: password, token: token})
    .done(function(data) {
      M.toast({html:"Reset successful"}, 4000)
      window.location.href="/login";
    })
    .fail(function(err) {
      M.toast({html:err.responseText}, 4000);
    })
  })
})

let submitResetPass = () => {
  let email = $('#email').val()
  $.post("/resetPassword", {email: email})
  .done(function(data) {
    M.toast({html:"An email with further instructions has been sent"}, 4000)
  })
  .fail(function(err) {
    M.toast({html:err.responseText}, 4000)
  })
}

let forgotPasswordForm = $('#forgotPasswordForm');
forgotPasswordForm.submit((e) => {
  e.preventDefault();
  submitResetPass();
})
