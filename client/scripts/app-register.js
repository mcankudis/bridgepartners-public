"use strict";
document.addEventListener("DOMContentLoaded", function(event) {
  let registerForm = document.getElementById('registerForm');
  console.log(registerForm);
  registerForm.addEventListener('submit', e => {
    e.preventDefault();
    let username = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let password2 = document.getElementById('password2').value;
    if(password!=password2) return M.toast({html:"Passwords do not match!"}, 4000);
    let data = {username, password, email};
    fetch("/register", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        referrer: "no-referrer",
        body: JSON.stringify(data)
    })
    .then(res => {
      if(!res.ok) res.text().then(text => M.toast({html:text}, 4000))
      else {
        M.toast({html:"Account created, You can now log in"}, 4000);
        window.location.href="/login";
      }
    });
  })
});
