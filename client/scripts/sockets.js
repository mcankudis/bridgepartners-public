"use strict";
let socketsListen = () => {
	socket.on('newBid', (data) => {
    if (currentChat._id==data.chat) {
      if(isWindowHidden()) notifyMe('New Bid', data.msg);
      M.toast({html:data.msg}, 4000);
      let i = boards.map(x => x._id).indexOf(data.board);
      let bid = data.bid;
      boards[i].sequence.push(bid);
      let x = getClassForBid(bid);
      let sign = x[0], c = x[1];
      let color;
      if(boards[i].sequence.length%2==1) color = 'red lighten-3';
      else color = 'yellow lighten-3';
      let bidO = `<li class="col s6 biddingBid ${color}">${bid[0]}<span class='${c}'>${sign} </span></li>`;
      $(`#auction${data.board}`).append(bidO);
    } else notifyMe('New Bid', data.msg);
  })
	socket.on('undoBid', (data) => {
    if (currentChat._id==data.chat) {
      M.toast({html:data.msg}, 4000);
      let i = boards.map(x => x._id).indexOf(data.board);
      boards[i].sequence.pop();
			$(`#auction${data.board}`).children().last().remove();
    }
  })
  socket.on('newBoard', (data) => {
    if (currentChat._id==data.chat) {
      if(isWindowHidden()) notifyMe('New Board',data.msg);
      $.get(`/boards/boardById/${data.board}`)
      .done(board => {
        boards.push(board);
        let b = boardToHtml(board);
        $('#noBoards').remove();
        $('#openAddBoard').removeClass('pulse');
        $('#boardList').append(b);
        $('.tooltipped').tooltip();
        M.toast({html:data.msg}, 4000);
      })
      .fail(err => handleErr(err))
    } else notifyMe('New Board', data.msg);
  })
  socket.on('endBoard', (data) => {
    if (currentChat._id==data.chat) {
      if(isWindowHidden()) notifyMe('Auction Over',data.msg);
      $.get(`/boards/boardById/${data.board}`)
      .done(board => {
        boards.splice(boards.map(x => x._id).indexOf(board._id),1);
        $('#boardCard'+board._id).remove();
        boards.push(board);
        let b = boardToHtml(board);
        $('#boardList').append(b);
        $('.tooltipped').tooltip();
        M.toast({html:data.msg}, 4000);
      })
      .fail(err => handleErr(err))
    } else notifyMe('Auction Over', data.msg);
  })
  socket.on('remBoard', (data) => {
    if (currentChat._id==data.chat) {
      if(isWindowHidden()) notifyMe('Board Removed', data.msg);

      let i = boards.map(x => x._id).indexOf(data.board);
      boards.splice(i, 1);
      $('#boardCard'+data.board).remove();
      M.toast({html:data.msg}, 4000);
    }
    else notifyMe('Board Removed',data.msg);
  })
  socket.on('arcBoard', (data) => {
    if (currentChat._id==data.chat) {
      let i = boards.map(x => x._id).indexOf(data.board);
      boards[i].archived = true;
      $('#boardCard'+data.board).fadeOut();
      M.toast({html:data.msg}, 4000);
    }
    else notifyMe('Board Removed',data.msg);
  })
}
