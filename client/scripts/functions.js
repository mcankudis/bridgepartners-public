"use strict";
let notification = "";

let addNewChat = (name, callback) => {
  return new Promise((resolve, reject) => {
    $.post("/chat/addChat", {
      username: name
    })
    .done(data => resolve(data))
    .fail(err => reject(err))
  })
}

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

let boardToHtml = (data) => {
  let room = "";
  room+= `<div class="col s12 m6" id="boardCard${data._id}">
            <div class="card blue-grey">
              <div class="card-content white-text">
                <div id="boardButtons${data._id}">
                  <a class="btn-floating waves-effect waves-light red darken-3 right tooltipped"
                  data-position="top" data-tooltip="Delete board" id="deleteButton${data._id}"
                  onclick="removeBoard('${data._id}')"><i class="material-icons">delete</i>
                  </a>`
                  if(!data.isOver) room+=
                  `<a class="btn-floating waves-effect waves-light yellow darken-2 right tooltipped marginR2"
                  data-position="top" data-tooltip="Undo last bid" id="undoButton${data._id}"
                  onclick="undoBid('${data._id}')"><i class="material-icons">undo</i></a>
                  <a class="btn-floating waves-effect waves-light green right tooltipped marginR2"
                  data-position="top" data-tooltip="End auction" id="overButton${data._id}"
                  onclick="endBoard('${data._id}')"><i class="material-icons">done</i></a>`
                  else if(!data.archived) room+=
                  `<a class="btn-floating waves-effect waves-light yellow darken-2 right tooltipped marginR2"
                  data-position="top" data-tooltip="Archive board" id="archiveButton${data._id}"
                  onclick="archiveBoard('${data._id}')"><i class="material-icons">archive</i>`
                  room+= `</a>
                </div>
                <div class="card-title">${data.title}</div>
                <div class="openBiddingBox" onclick="openBiddingBox('${data._id}')">
                  <div class="row">
                    <div class="col s12 m6 marginB1">`
                      if(data.isOver) room+=`<div>${data.hand.pos.toUpperCase()}</div>`
                      room+=`<div><span class="spade">&#9824;</span> ${data.hand.s}</div>
                      <div><span class="heart">&#9829;</span> ${data.hand.h}</div>
                      <div><span class="diamond">&#9830;</span> ${data.hand.d}</div>
                      <div><span class="club">&#9827;</span> ${data.hand.c}</div>
                    </div>
                    <div id="otherHand${data._id}"></div>`
                    if(data.isOver) {
                      room+=`
                      <div class="col s12 m6">
                      <div>${data.hand2.pos.toUpperCase()}</div>
                        <div><span class="spade">&#9824;</span> ${data.hand2.s}</div>
                        <div><span class="heart">&#9829;</span> ${data.hand2.h}</div>
                        <div><span class="diamond">&#9830;</span> ${data.hand2.d}</div>
                        <div><span class="club">&#9827;</span> ${data.hand2.c}</div>
                      </div>`;
                    }
                    if(data.isOver) room+=`<div class="col s12">
                    <ul class="black-text row" id="auction${data._id}">`
                    else room+=`<div class="col s12 m6">
                      <ul class="black-text row" id="auction${data._id}">`
                      if(data.dealer=='n') {
                        let n = `<li class="col s6 biddingHeading red lighten-3 `;
                        if(data.myPosition=='n') n+= `myPosition`;
                        n+=`">N</li>`;
                        let s = `<li class="col s6 biddingHeading yellow lighten-3 `;
                        if(data.myPosition=='s') s+= `myPosition`;
                        s+=`">S</li>`;
                        room+=n+s;
                      }
                      if(data.dealer=='s') {
                        let n = `<li class="col s6 biddingHeading yellow lighten-3 `;
                        if(data.myPosition=='n') n+= `myPosition`;
                        n+=`">N</li>`;
                        let s = `<li class="col s6 biddingHeading red lighten-3 `;
                        if(data.myPosition=='s') s+= `myPosition`;
                        s+=`">S</li>`;
                        room+=s+n;
                      }
                      for (var j = 0; j < data.sequence.length; j++) {
                        let color;
                        if(j%2==0) color = 'red lighten-3';
                        else color = 'yellow lighten-3';
                        let x = getClassForBid(data.sequence[j]);
                        let sign = x[0], c = x[1];
                        let bidO = `<li class="col s6 biddingBid ${color}">
                          ${data.sequence[j][0]}<span class='${c}'>${sign} </span>
                        </li>`;
                        room+=bidO
                      }
                      room+=
                        `</ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>`
  return room;
}

let getClassForBid = (bid) => {
  let sign = '',c;
  switch (bid[1]) {
    case 'c':
      sign = '♣';
      c = 'club';
      break;
    case 'd':
      sign = '♦';
      c = 'diamond';
      break;
    case 'h':
      sign = '♥';
      c = 'heart';
      break;
    case 's':
      sign = '♠';
      c = 'spade';
      break;
    case 'n':
      sign = 'NT';
      c = 'nt'
      break;
    case 'x':
      sign = 'x';
      c = 'xx;'
      break;
    case 'a':
      sign = 'ass';
      c = 'pass'
      break;
    default:
      c = 'x'
  }
  return [sign, c];
}

let getChats = () => {
  $.get("/chat")
  .done(data => {
    chats = data;
    refreshChats(chats);
  })
  .fail(err =>
    handleErrAsync(err)
    .then(code => code===2 ? getChats() : true)
  )
}

let handleErrAsync = (err) => {
  return new Promise((resolve, reject) => {
    if(err.responseText==="Forbidden") {
      return reissueToken()
      .then(()=>resolve(2))
      .catch(()=> logout())
    }
    if(err.responseText) M.toast({html:err.responseText}, 4000)
    else M.toast({html:"Network Error"}, 4000)
    resolve(1)
  })
}

let isWindowHidden = () =>
  (typeof document.hidden !== "undefined" && document.hidden===true)
  || typeof document.msHidden !== "undefined"
  || typeof document.webkitHidden !== "undefined"

let logout = () => {
  M.toast({html:"Logging out"}, 4000)
  localStorage.clear();
  window.location.href = "/login";
}

let notifyMe = (title, msg) => {
  if (!("Notification" in window))
    alert("This browser does not support desktop notification");
  else if (Notification.permission === "granted")
    spawnNotification(title, msg, 'bridgePartners');
  else if (Notification.permission !== "denied")
    Notification.requestPermission()
    .then(permission => {if(permission === "granted") spawnNotification(title, msg, 'bridgePartners')});
}

let reissueToken = () => {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "/reissue",
      type: 'GET',
      headers: {'x-reissue': localStorage.getItem('reissueToken')},
    })
    .then(data => {
      localStorage.setItem('token', data.token);
      localStorage.setItem('reissueToken', data.reissueToken);
      $.ajaxSetup({
        headers: { 'x-auth': data.token }
      });
      resolve();
    })
    .fail(err => {
      console.log(err);
      reject();
    })
  })
}

let setHeaders = headers => {
  let token = localStorage.getItem('token');
  if(token) return {...headers, 'x-auth': token};
  else return headers;
}

let spawnNotification = (title, msg, tag) => {
  let options = {
    body: msg,
    icon: "images/favicon.png",
    tag: tag,
  }
  notification = new Notification(title, options);
}
