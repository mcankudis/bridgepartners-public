let sockets = {};

sockets.init = (server) => {
  // socket.io setup
  let users = [],
  connections = [];
  var io = require('socket.io').listen(server);
  io.sockets.on('connection', socket => {

    connections.push(socket);
    console.log(connections.length);
    socket.on('attachUser', username => users[username] = socket.id)

    module.exports.bid = function(body, from) {
      let to = body.to,
      board = body.board,
      bid = body.bid,
      chat = body.chat;
      if(to==from) return;
      to = users[to];
      console.log(body);
      console.log(users);
      io.to(`${to}`).emit('newBid', {msg: `Your partner ${from} just made a bid!`, board: board, bid: bid, chat: chat});
    }
    module.exports.addBoard = function(board, from, to, chat) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('newBoard', {msg: `Your partner ${from} just added a board!`, board: board, chat: chat});
    }
    module.exports.addChat = function(from, to) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('addChat', {msg: `${from} just started a chat with You!`});
    }
    module.exports.archiveBoard = function(board, from, to, chat) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('arcBoard', {msg: `Your partner ${from} just archived a board!`, board: board, chat: chat});
    }
    module.exports.endBoard = function(board, from, to, chat) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('endBoard',
      {msg: `An auction with Your partner ${from} has just came to an end!`, board: board, chat: chat});
    }
    module.exports.removeBoard = function(board, from, to, chat) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('remBoard', {msg: `Your partner ${from} just removed a board!`, board: board, chat: chat});
    }
    module.exports.undoBid = function(board, from, to, chat) {
      if(to==from) return;
      to = users[to];
      io.to(`${to}`).emit('undoBid', {msg: `Your partner ${from} just took back a bid!`, board: board, chat: chat});
    }

    // Disconnect
    socket.on('disconnect', (data) => {
      connections.splice(connections.indexOf(socket), 1);
      console.log(connections.length);
    });
    
  })
}

module.exports = sockets;
