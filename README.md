## BridgePartners

##### [Open in browser](bp.potikreff.pl)

BridgePartners is a simple web app for bridge players who want to train their bidding skills with their partners. 
It functions similairly to a regular chat app using websockets, yet the communication runs in form of bridge bids. 
The app uses push-notifications to let players know when their partner makes a bid. 
It's also available as a Progressive Web App.


In case You want to run the app on Your device, You need to have nodeJS v11+ and mongodb installed.
Installation steps:

1. Clone the repo to a directory of choice
2. Create a .env file in repo's root folder and paste in following content, replacing comments with necessary data: <br/>
SECRET=*just a random string* <br/>
SECRET2=*just a random string* <br/>
REISSUE_SECRET=*just a random string* <br/>
GUEST_SECRET=*just a random string* <br/>
MAIL=*your email to be used as a subject in webpush subscription*<br/>
MAIL_HOST=*mail.example.com*<br/>
MAIL_USER=*mail@example.com*<br/>
MAIL_PASSWORD=*mail_password*<br/>
Optional enviromental variables:<br/>
PORT=*the port for the app to run on, default is 3000*<br/>
DB_PATH=*the path to mongodb, for example mongodb://localhost/partners*<br/>
DB_USER=*dabase username in case you've enabled authorization*<br/>
DB_PASS=*dabase password in case you've enabled authorization*<br/>
3. Navigate there in command line
4. Start the mangodb service<br/>
If You're running a debian-based distro type in: `sudo service mongod start`<br/>
If You're running an arch-based distro type in: `sudo systemctl start mongodb`
5. Run following commands: <br/>
`npm install` <br/>
`npm start` <br/>
5. The app should be running on localhost:3000 or your port of choice

Warning: when using `npm install` node will automatically install the newest versions of the packages and also update the package.json file. In case You experience any compabilty issues look up the initial version in package.json on github or just redownload the repo, remove the '^' in incompatibile package version and run `npm update` / `npm install`

Also the password recovery service is not going to work on a local machine.
