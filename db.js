"use strict";
const mongoose = require('mongoose')
const dbpath = process.env.DB_PATH || 'mongodb://localhost/bridgePartners';

mongoose.connect(dbpath, {user: process.env.DB_USER, pass: process.env.DB_PASS, useNewUrlParser: true}, err => {
    if(err) console.error(err);
    else console.log('Nawiązano połączenie z bazą');
  }
)

module.exports = mongoose
