'use strict';
const express = require('express');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const passport = require('passport');
const router = express.Router();
const webpush = require('web-push');

// DB Models
const User = require('../models/user');
const PushSubscription = require('../models/pushSubscription');

// Middleware/functions
const keys = require('../config/keys');
const sanitize = require('../config/sanitize');
const functions = require('../functions/other');
const ensureAuthenticated = require('../functions/ensureAuthenticated');
const getRandomString = functions.getRandomString;

const vapidKeys = webpush.generateVAPIDKeys()
console.log(vapidKeys);
webpush.setVapidDetails(`mailto:${process.env.MAIL}`,vapidKeys.publicKey, vapidKeys.privateKey);

router.get('/',         (req, res) => res.render('views/index.html'));
router.get('/login',    (req, res) => res.render('views/login.html'));
router.get('/register', (req, res) => res.render('views/register.html'));
router.get('/guest',    (req, res) => res.render('views/guest.html'));

router.get('/ping',     (req, res) => res.sendStatus(200));

// WebPush subscription
router.post('/subscribe', ensureAuthenticated, (req, res) => {
  const sub = req.body;
  PushSubscription.updateOne(
    { user_id: req.auth.id },
    {
      $set: {
      username: req.auth.username,
      user_id: req.auth.id,
      subscription: sub
      }
    },
    { upsert: true },
    (err, ps) => {
    if(err) {
      console.error(err);
      return res.status(404).json({});
    }
    res.status(201).json({});
  })
})

router.get('/publicVapidKey', (req, res) => {
  res.send(vapidKeys.publicKey);
})

// Register User
router.post('/register', (req,res,next) => {
  let email = req.body.email;
  let username = req.body.username;
  let password = req.body.password;
  if(!sanitize.verifyString(email)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(username)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(password)) return res.status(401).send("Invalid data");
  let newUser = new User({
    email: email,
    username: username,
    password: password,
    reissue_id: getRandomString(20, 30)
  });
  User.createUser(newUser)
  .then(user=> res.redirect("/login"))
  .catch(err => {
    if(err.code == "11000") {
      if(err.errmsg.includes("email")) return res.status(418).send("Email already taken");
      return res.status(418).send("Username already taken");
    }
    else return res.status(404).send("Unknown error");
  })
});

// Login
router.post('/login',
(req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    console.log(err, user, info);
    if (err) return next(err);
    if (!user) {
      if(info.message=="NoUser") return res.status(401).send("User not found");
      else if(info.message=="WrongPassword") return res.status(401).send("Wrong password");
      else if(info.message=="Missing credentials") return res.status(401).send("Missing credentials");
      else return res.status(401).send("Unknown error");
    }
    req.locals = {user};
    next();
  })(req, res, next);
},
(req, res, next) => {
  User.byId(req.locals.user._id)
  .then(user => {
    req.locals.user = user;
    next();
  })
  .catch(err=>res.status(err.code).json(err.msg))
},
(req, res) => {
  let user = req.locals.user;
  let random = getRandomString(40, 60);
  let token = jwt.sign({
    username: user.username,
    id: user._id,
    sub: random
  }, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
  random = getRandomString(40, 60);
  let reissueToken = jwt.sign({
    id: user._id,
    sub: random,
    validCheck: user.reissue_id
  }, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});

  res.json({
    token,
    reissueToken,
    user: {
      username: user.username,
      _id: user._id
    }
  });
});

// Reissue tokens
router.get('/reissue',
(req, res, next) => {
  console.log(req.headers);
  if(!sanitize.verifyString(req.headers['x-auth'])) return res.status(401).send("Unauthorized");
  if(!sanitize.verifyString(req.headers['x-reissue'])) return res.status(401).send("Unauthorized");
  if(!req.headers['x-reissue']) return res.sendStatus(401);
  jwt.verify(req.headers['x-reissue'], process.env.REISSUE_SECRET, (err, token) => {
    if(err || !token) return res.sendStatus(401);
    req.locals = {token};
    next();
  });
},
(req, res, next) => {
  User.byId(req.locals.token.id)
  .then(user => {
    req.locals.user = user;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res) => {
  let user = req.locals.user;
  if(req.locals.token.validCheck!==user.reissue_id) return res.sendStatus(401);
  let random = getRandomString(40, 60);
  let token = jwt.sign({
    username: user.username,
    id: user._id,
    sub: random
  }, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
  random = getRandomString(40, 60);
  let reissueToken = jwt.sign({
    id: user._id,
    sub: random,
    validCheck: user.reissue_id
  }, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});
  res.json({token, reissueToken});
})

// Get user by id
router.get('/users/:id', ensureAuthenticated, (req, res) => {
  let id = req.params.id;
  if(!sanitize.verifyWord(id)) return res.sendStatus(401);
  User.findOne({_id: id}).select('username').select('_id').exec((err, user) => {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

// Get user by name
router.get('/user/:name', ensureAuthenticated, (req, res) => {
  let username = req.params.name;
  if(!sanitize.verifyString(username)) return res.sendStatus(401);
  User.findOne({username: username}).select('username').select('_id').exec((err, user) => {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

// Change password
router.post('/changePassword', ensureAuthenticated, function(req, res, next) {
  req.body.username = req.auth.username;
  if(!sanitize.verifyString(req.body.newPassword)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.password)) return res.sendStatus(401);
  passport.authenticate('local', function(err, user, info) {
    if (err) return res.status(401).send("An error occured");
    if (!user) return res.status(401).send("User not found");
    User.changePasswd(user._id, req.body.newPassword, function(err, suc) {
      if(err) return res.status(401).send("An error occured");
      return res.status(200).send("Password changed");
    })
  })(req, res, next);
})

// Change password
router.post('/changeUsername', ensureAuthenticated, function(req, res, next) {
  req.body.username = req.auth.username;
  if(!sanitize.verifyString(req.body.newUsername)) return res.status(401).send('Invalid username');
  if(!sanitize.verifyString(req.body.password)) return res.status(401).send('Invalid password');
  passport.authenticate('local', function(err, user, info) {
    if (err) return res.status(401).send("An error occured");
    if (!user) return res.status(401).send("User not found");
    User.changeUsername(user._id, req.body.newUsername, function(err, suc) {
      if(err) return res.status(401).send("An error occured");
      let random = getRandomString(10, 100);
      let token = jwt.sign({
        username: req.body.newUsername,
        id: req.auth.id,
        sub: random
      }, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});
      return res.status(200).json({username: req.body.newUsername, token});
    })
  })(req, res, next);
})

// Reset password
router.post('/resetPassword', function(req, res) {
  console.log(req.body);
  if(!sanitize.verifyString(req.body.email)) return res.sendStatus(401);
  User.findOne({email: req.body.email}, function(err,user) {
    if(err) return res.status(401).send("An error occured");
    if(!user) return res.status(404).send("Account not found");
    let random = getRandomString(40, 100);
    let token =
      jwt.sign({ username: user.username, id: user._id, sub: random}, process.env.SECRET2, {expiresIn: '5m'});
    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: 587,
      secure: false,
      auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASSWORD
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    let text, subject;
      text = `<b>Hello ${user.username}</b>
      <p>Someone, probably You, requestet a password reset for Your BridgePartners account.
      Click the link below to enter a new password. Please notice that the link is only active for 5 minutes.</p>
      <a href="${process.env.SERVER_PATH}/reset/${token}">
        Password reset</a><p>In case it was not You who requested a password reset,
        please contact our support at <a href="mailto:mikolaj@cankudis.net">this email adress
      </a></p>`;
      subject = "Password reset";
    let mailOptions = {
        from: '"noreply" <noreply@bridgePartners.potikreff.pl>',
        to: req.body.email,
        subject: subject,
        html: text
    };
    transporter.sendMail(mailOptions, (error, info) => {
      console.log(info);
      if (error) return res.status(401).send("An erro occured");
      res.sendStatus(201);
    });
  })
})

router.get('/reset/:seq', function(req, res) {
  let token = req.params.seq;
  jwt.verify(token, process.env.SECRET2, function(err, token) {
    if(err) return res.sendStatus(401);
    else res.render('views/changepass.html');
  });
})

router.post('/reset/resetChangePass', function(req,res) {
  if(!sanitize.verifyString(req.body.password)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.token)) return res.sendStatus(401);
  jwt.verify(req.body.token, process.env.SECRET2, function(err, user) {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(401);
    User.changePasswd(user.id, req.body.password, function(err, suc) {
      if(err) return res.sendStatus(401);
      console.log(process.env.SECRET2);
      keys.tokenAuth.rerollSecret2();
      console.log(process.env.SECRET2);
      return res.sendStatus(201);
    })
  })
})


module.exports = router
