'use strict';
const ensureAuthenticated = require('../functions/ensureAuthenticated')
const express = require('express')
const router = express.Router()
const Chat = require('../models/chat');
const User = require('../models/user');
const sanitize = require('../config/sanitize');
const sockets = require('../websockets/socketio.js');
const functions = require('../functions/other');
const isUserInChat = functions.isUserInChat;

router.get('/', ensureAuthenticated, function(req, res, next) {
  Chat.find({players: req.auth.id}, function(err, chats) {
    if(err) return res.status(401).send("Unknown error");
    if(!chats) return res.json({});
    chats = JSON.parse(JSON.stringify(chats));
    if(chats.length===0) return res.json({});
    let promises = chats.map(chat => {
      return chat.players.map(player => {
        return new Promise(function(resolve, reject) {
          User.getUserById(player, function(err, user) {
            if(err) return reject(err);
            if(!user) return reject(null);
            resolve(user);
          });
        });
      })
    })
    promises = promises.flat();
    Promise.all(promises)
    .then(function(players) {
      let playersIdToName = [];
      players = JSON.parse(JSON.stringify(players));
      for(let i = 0; i<players.length; i++) {
        playersIdToName[players[i]._id] = {_id: players[i]._id, username: players[i].username}
      }
      for (var i = 0; i < chats.length; i++) {
        chats[i].players[0] = playersIdToName[chats[i].players[0]];
        chats[i].players[1] = playersIdToName[chats[i].players[1]];
        let withWhom;
        if(chats[i].players[0]._id==req.auth.id) withWhom = chats[i].players[1].username;
        else withWhom = chats[i].players[0].username;
        if(!chats[i].customName) chats[i].customName = withWhom;
        chats[i].partner = withWhom;
      }
      return res.json(chats);
    })
    .catch(console.error)
  })
})

router.post('/addChat', ensureAuthenticated, function(req, res, next) {
  if(!sanitize.verifyString(req.body.username)) return res.status(401).send("Invalid data");
  let username = req.body.username;
  if(username==req.auth.username) return res.status(404).send("You can't start an auction with Yourself")
  User.getUserByUsername(username, function(err, user) {
    if(err) return res.status(401).send("Unknown error");
    if(!user) return res.status(404).send("User not found");
    else {
      let newChat = new Chat({
        players: [req.auth.id, user.id],
        pId_1: req.auth.id + user.id,
        pId_2: user.id + req.auth.id
      })
      newChat.save(function(err, chat) {
        if(err) {
          if(err.code===11000) return res.status(401).send("You already started a room with this player")
          return res.status(401).send("Unknown error");
        }
        chat.players[0] = {_id: req.auth.id, username: req.auth.username};
        chat.players[1] = {_id: user.id, username: req.body.username};
        let withWhom;
        if(chat.players[0]._id==req.auth.id) withWhom = chat.players[1].username;
        else withWhom = chat.players[0].username;
        if(!chat.customName) chat.customName = withWhom;
        chat.partner = withWhom;
        sockets.addChat(req.auth.username, req.body.username);
        res.json(chat);
      })
    }
  })
})

router.post('/quit', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyString(req.body._id)) return res.status(401).send("Invalid data");
  req.locals = {};
  return next();
},
(req, res, next) => {
  Chat.findById(req.body._id)
  .then(chat => {
    chat.players.slice(chat.players.indexOf(req.auth.id), 1);
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
isUserInChat,
(req, res) => {
  Chat.updateOne({_id: req.body._id}, {$set: {players: req.locals.chat.players}}, function(err) {
    if(err) return res.status(401).send("Unknown error");
    res.status(200).send("You left the room");
  })
})

module.exports = router
