'use strict';
// node_modules
const express = require('express');
const router = express.Router();
const request = require('request');
const cheerio = require('cheerio');
const jwt = require('jsonwebtoken');
const webpush = require('web-push');

// DB Models with methods
const Chat = require('../models/chat');
const Board = require('../models/board');
const User = require('../models/user');
const PushSubscription = require('../models/pushSubscription');

// Middleware/functions
const sanitize = require('../config/sanitize');
const sockets = require('../websockets/socketio.js');
const ensureAuthenticated = require('../functions/ensureAuthenticated');
const functions = require('../functions/other');

const findChat = Chat.findById;
const findBoardsByChat = Board.findByChat;
const findBoard = Board.findById;
const prepareBoardsForPlayers = Board.prepareForPlayer;
const boardFromHands = Board.createFromHands;
const isUserInChat = functions.isUserInChat;
const getRandomString = functions.getRandomString;

router.get('/boardsByChatId/:id', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(401).json("Invalid data");
  req.locals = {};
  next();
},
(req, res, next) => {
  findChat(req.params.id)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err => res.status(err.code).json(err.msg))
},
isUserInChat,
(req, res, next) => {
  findBoardsByChat(req.locals.chat._id)
  .then(boards => {
    req.locals.boards = boards;
    next();
  })
  .catch(err=>res.status(err.code).json(err.msg))
},
(req, res) => {
  let boards = JSON.parse(JSON.stringify(req.locals.boards));
  boards = prepareBoardsForPlayers(boards, req.auth.id);
  res.status(200).json(boards);
})

router.post('/addBid', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.body.board)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(req.body.bid)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(req.body.chat)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(req.body.to)) return res.status(401).send("Invalid data");
  req.locals = {};
  return next();
},
(req, res, next) => {
  findBoard(req.body.board)
  .then(board => {
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res) => {
  let board = req.locals.board;
  if(board.isOver || board.archived) return res.status(404).send("This board is over");
  let whoNow;
  if(board.sequence.length%2===0) whoNow = board.dealer;
  else whoNow = board.otherPos;
  if(board.hands.n.player===req.auth.id) {
    if(whoNow!=="n") return res.status(404).send("It's not Your turn");
  }
  else if(board.hands.s.player===req.auth.id) {
    if(whoNow!=="s") return res.status(404).send("It's not Your turn");
  }
  else return res.sendStatus(401);
  Board.updateOne({_id: req.body.board}, {$push: {sequence: req.body.bid}}, function(err) {
    if(err) return res.status(401).send("Unknown error");
    sockets.bid(req.body, req.auth.username);
    res.json({id: board._id});
    PushSubscription.findByUsername(req.body.to)
    .then(sub => {
      const payload = JSON.stringify({title: "New bid",  msg: `Your partner ${req.auth.username} just made a bid.`});
      webpush.sendNotification(sub.subscription, payload)
      .then(x => console.log(x))
      .catch(err => console.error(err))
    })
    .catch(err => console.error(err))
  })
})

router.post('/undo', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.body.board)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(req.body.chat)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(req.body.to)) return res.status(401).send("Invalid data");
  req.locals = {};
  return next();
},
(req, res, next) => {
  findBoard(req.body.board)
  .then(board => {
    board.sequence.pop();
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res) => {
  let board = req.locals.board;
  if(board.isOver || board.archived) return res.status(404).send("This board is over");
  Board.updateOne({_id: req.body.board}, {$set: {sequence: req.locals.board.sequence}}, (err) => {
    if(err) return res.status(401).send("Unknown error");
    sockets.undoBid(req.body.board, req.auth.username, req.body.to, req.body.chat);
    return res.json({id: board._id});
  })
})

router.post('/end', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.body.board)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(req.body.chat)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(req.body.to)) return res.status(401).send("Invalid data");
  req.locals = {};
  return next();
},
(req, res, next) => {
  findBoard(req.body.board)
  .then(board => {
    if(board.isOver) return res.status(404).send("Auction is already over");
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res, next) => {
  findChat(req.locals.board.chat)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
isUserInChat,
(req, res) => {
  let board = req.locals.board;
  Board.updateOne({_id: board._id}, {$set: {isOver: true}}, (err, status) => {
    if(err) return res.status(404).send("Unknown error");
    sockets.endBoard(board._id, req.auth.username, req.body.to, req.body.chat);
    PushSubscription.findByUsername(req.body.to)
    .then(sub => {
      const payload = JSON.stringify({title: "Auction over", msg: `An auction with ${req.auth.username} just came to the end.`});
      webpush.sendNotification(sub.subscription, payload)
      .then(x => console.log(x))
      .catch(err => console.error(err))
    })
    .catch(err => console.error(err))
    if(board.hands.n.player == req.auth.id) return res.json({hand: board.hands.s, _id: board._id});
    return res.json({hand: board.hands.n, _id: board._id});
  })
})

router.post('/new',
ensureAuthenticated,
functions.verifyHand,
(req, res, next) => {
  findChat(req.body.chat)
  .then(chat => {
    req.locals = {};
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
}, isUserInChat,
(req, res) => {
  let chat = req.locals.chat
  req.auth.i = chat.players.indexOf(req.auth.id);
  let otherPlayer = chat.players[1-req.auth.i];
  let formData = {
    BOARDS: 1,
    button_gen_set2hands: "+Generate+",
    H1: 0,
    H2: 2,
    CMAX1:	req.body.CMAX1,
    CMAX2:	req.body.CMAX2,
    CMIN1:	req.body.CMIN1,
    CMIN2:	req.body.CMIN2,
    DMAX1:	req.body.DMAX1,
    DMAX2:	req.body.DMAX2,
    DMIN1:	req.body.DMIN1,
    DMIN2:	req.body.DMIN2,
    HMAX1:	req.body.HMAX1,
    HMAX2:	req.body.HMAX2,
    HMIN1:	req.body.HMIN1,
    HMIN2:	req.body.HMIN2,
    PMAX1:	req.body.PMAX1,
    PMAX2:	req.body.PMAX2,
    PMIN1:	req.body.PMIN1,
    PMIN2:	req.body.PMIN2,
    SMAX1:	req.body.SMAX1,
    SMAX2:	req.body.SMAX2,
    SMIN1:	req.body.SMIN1,
    SMIN2:	req.body.SMIN2
  }
  request.post({
    url:'http://playbridge.com/pb_gen_list.php',
    formData: formData
  }, function(err, httpResponse, body) {
    if(err) {
      console.error('upload failed:', err);
      return res.status(404).send("Failed to generate board")
    }
    let $ = cheerio.load(body);
    let b = $('table.center').html();
    $ = cheerio.load(b)
    let hands = [];
    $('.nobr').each(function() {
      let a = $(this).html()
      hands.push(a.slice(33, a.length))
    })
    let board = boardFromHands(hands, req.body.chat, req.body.title);
    if(req.body.position==="n") {
      board.hands.n.player = req.auth.id;
      board.hands.s.player = otherPlayer;
      board.dealer = "n";
      board.otherPos = "s";
    } else if(req.body.position==="s") {
      board.hands.n.player = otherPlayer;
      board.hands.s.player = req.auth.id;
      board.dealer = "s";
      board.otherPos = "n";
    } else {
      return res.status(401).send("Enter a correct position")
    }
    board.save((err, board) => {
      board = JSON.parse(JSON.stringify(board));
      if(err) return res.status(401).send("An error occured");
      prepareBoardsForPlayers([board], req.auth.id);
      sockets.addBoard(board._id, req.auth.username, req.body.to, req.body.chat);
      PushSubscription.findByUsername(req.body.to)
      .then(sub => {
        const payload = JSON.stringify({title: "New board", msg: `Your partner ${req.auth.username} just added a new board.`});
        webpush.sendNotification(sub.subscription, payload)
        .then(x => console.log(x))
        .catch(err => console.error(err))
      })
      .catch(err => console.error(err))
      return res.json(board);
    });
  });
})

router.post('/remove',
ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.body.board)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(req.body.to)) return res.status(401).send("Invalid data");
  next();
},
(req, res, next) => {
  findBoard(req.body.board)
  .then(board => {
    req.locals = {};
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res, next) => {
  findChat(req.locals.board.chat)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
isUserInChat,
(req, res) => {
  let board = req.locals.board;
  Board.deleteOne({_id: board._id}, function(err, n) {
    if(err) res.status(404).send("Unknown error");
    sockets.removeBoard(board._id, req.auth.username, req.body.to, req.locals.chat._id);
    PushSubscription.findByUsername(req.body.to)
    .then(sub => {
      const payload = JSON.stringify({title: "Board removed", msg: `Your partner ${req.auth.username} just removed a board.`});
      webpush.sendNotification(sub.subscription, payload)
      .then(x => console.log(x))
      .catch(err => console.error(err))
    })
    .catch(err => console.error(err))
    return res.status(200).send("Board removed");
  })
})

router.post('/archive', ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.body.board)) return res.status(401).send("Invalid data");
  next();
},
(req, res, next) => {
  findBoard(req.body.board)
  .then(board => {
    req.locals = {};
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res, next) => {
  findChat(req.locals.board.chat)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
isUserInChat,
(req, res) => {
  let board = req.locals.board
  Board.updateOne({_id: board._id}, {$set: {archived: true}}, function(err, status) {
    sockets.archiveBoard(board._id, req.auth.username, req.body.to, req.body.chat)
    return res.status(200).send("Archived");
  })
})

router.get('/boardById/:id',
ensureAuthenticated,
(req, res, next) => {
  if(!sanitize.verifyWord(req.params.id)) return res.status(401).json("Invalid data");
  next();
},
(req, res, next) => {
  findBoard(req.params.id)
  .then(board => {
    req.locals = {};
    req.locals.board = board;
    next();
  })
  .catch(err=>res.status(err.code).json(err.msg))
},
(req, res, next) => {
  findChat(req.locals.board.chat)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).json(err.msg))
},
isUserInChat,
(req, res) => {
  let board = JSON.parse(JSON.stringify(req.locals.board));
  board = prepareBoardsForPlayers([board], req.auth.id);
  res.json(board[0]);
})

// Grant guest access to a chat via link
router.post('/guest',
(req, res, next) => {
  let token = req.body.token;
  if(!token) return res.status(401).send("Invalid data");
  if(!sanitize.verifyString(token)) return res.status(401).send("Invalid data");
  if(!token || token == '') return res.status(401).send("Invalid data");
  jwt.verify(token, process.env.GUEST_SECRET, {algorithms: ['HS512']}, (err, data) => {
    if(err) {
      console.error(err);
      return res.status(401).send("Unauthorized");
    }
    req.locals = {};
    req.locals.data = data;
    next();
  })
},
(req, res, next) => {
  findChat(req.locals.data._id)
  .then(chat => {
    req.locals.chat = chat;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res, next) => {
  let chat = req.locals.chat;
  req.locals.promises = chat.players.map(player => {
    return new Promise(function(resolve, reject) {
      User.getUserById(player, function(err, user) {
        if(err) return reject(err);
        if(!user) return reject(null);
        resolve(user);
      });
    });
  })
  next();
},
(req, res, next) => {
  findBoardsByChat(req.locals.chat._id)
  .then(boards => {
    req.locals.boards = boards;
    next();
  })
  .catch(err=>res.status(err.code).send(err.msg))
},
(req, res) => {
  let data = req.locals.data;
  let boards = JSON.parse(JSON.stringify(req.locals.boards));
  if(data.access_lvl=="archived") boards = boards.filter(board => board.archived);
  if(data.access_lvl=="active") boards = boards.filter(board => !board.archived);
  Promise.all(req.locals.promises)
  .then(function(users) {
    let p = users.map(user => {
      return {
        _id: user._id,
        username: user.username
      }
    })
    return res.json({boards: boards, players: p});
  })
  .catch(err => console.error(err))
})

router.post('/generateGuestLink', ensureAuthenticated, (req, res) => {
  let chat = req.body.chat,
  exp = req.body.exp,
  access_lvl = req.body.acc;
  if(!sanitize.verifyString(chat)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(exp)) return res.status(401).send("Invalid data");
  if(!sanitize.verifyWord(access_lvl)) return res.status(401).send("Invalid data");
  if(exp==0 || !exp) exp = "9999d";
  let random = getRandomString(10, 100);
  let token = jwt.sign({
    _id: chat,
    sub: random,
    access_lvl: access_lvl
  }, process.env.GUEST_SECRET, {algorithm: 'HS512', expiresIn: exp});
  res.json("localhost:3000/guest?token="+token);
})

module.exports = router;
